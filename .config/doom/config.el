;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;;;; Helper functions
(defun slurp (f)
  (with-temp-buffer
    (insert-file-contents f)
    (buffer-substring-no-properties
     (point-min) (point-max))))


;;;; Config

;; Miscellaneous
(setq tmp-user-data
      (split-string
       (slurp (substitute-in-file-name "$XDG_DATA_HOME/user"))
       "\n" t))
(setq user-full-name (nth 0 tmp-user-data)
      user-mail-address (nth 1 tmp-user-data))

;; Doom
(setq doom-theme 'doom-snazzy
      doom-font (font-spec :family "Iosevka Simsva" :size 14)
      ;;doom-variable-pitch-font (font-spec :family "Fira Sans") ; inherits `doom-font''s :size
      doom-unicode-font (font-spec :family "JoyPixels" :size 12)
      doom-big-font (font-spec :family "Iosevka Simsva" :size 19))

;; Text editing
(setq display-line-numbers-type t)

;; Projectile
(setq projectile-project-search-path
      (list
       (substitute-in-file-name "$SHORTCUT_DIR_SRC")
       (cons (substitute-in-file-name "$SHORTCUT_DIR_DEV") 2)
       (substitute-in-file-name "$HOME")
       ))

;; Org
(setq org-directory (substitute-in-file-name "$XDG_DOCUMENTS_DIR/org/"))
(setq org-hide-emphasis-markers :true)

;; LSP
(after! ccls
  (setq ccls-initialization-options '(:index (:comments 2) :completion (:detailedLabel t)))
  (set-lsp-priority! 'ccls 2)) ; optional as ccls is the default in Doom
(setq lsp-lens-enable nil)

(setq gdscript-debug-port 6005)


;;;; Modes

;; Whitespace
(global-whitespace-mode +0)
(setq whitespace-style '(face tabs tab-mark spaces space-mark trailing newline newline-mark)
      whitespace-display-mappings '(
        (space-mark     ?\      [?\u00B7]       [?.])
        (space-mark     ?\xA0   [?\u00A4]       [?_]) ;; No-Break Space
        (newline-mark   ?\n     [?¬ ?\n])
        (tab-mark       ?\t     [?\u00BB ?\t]   [?\\ ?\t])))


;;;; Keybinds

(map! :leader
      :desc "Toggle whitespace-mode"
      "t W" #'whitespace-mode)
